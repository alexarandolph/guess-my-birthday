from random import randint

# ask for user's name
name = input("Hi! What is your name? ")
# Guess sequence
for guess_number in range(1,6):
    guess_month = randint(1, 12)
    guess_year = randint(1924, 2004)

    print("Guess", guess_number, ": ", "were you born in",
    guess_month, "/", guess_year, "?")
    # then prompts with yes or no
    response = input("yes or no? ")
    # if computer guesses correctly
    if response == "yes":
        # print message "I knew it!"
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Good bye.")
    # if incorrect
    else:
        # respond "Drat! Lemme try again!"
        print("Drat! Lemme try that again!")
